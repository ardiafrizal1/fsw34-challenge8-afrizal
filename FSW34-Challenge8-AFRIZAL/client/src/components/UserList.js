import React, { useContext } from 'react';
import { GlobalContext } from '../context/GlobalState';
import { Link } from 'react-router-dom';
import { ListGroup, Button } from 'reactstrap';

export const UserList = () => {
  const { users, removeUser } = useContext(GlobalContext);

  return (
    <ListGroup className="mt-4">
      {users.length > 0 ? (
        <>
          <table border="1">
            <thead>
              <tr>
                <th>Username</th>
                <th>Email</th>
                <th>Level</th>
                <th>Experience</th>
                <th>Edit or Delete</th>
              </tr>
            </thead>
            <tbody>
              {users.map((user) => (
                <tr key={user.id}>
                  <td>
                    <strong>{user.name}</strong>
                  </td>
                  <td>
                    <strong>{user.email}</strong>
                  </td>
                  <td>
                    <strong>{user.lvl}</strong>
                  </td>
                  <td>
                    <strong>{user.experience}</strong>
                  </td>
                  <td>
                    <div className="ml-auto">
                      <Link to={`/edit/${user.id}`} color="warning" className="btn btn-warning "></Link>
                      <Button onClick={() => removeUser(user.id)} color="danger">
                        Delete
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </>
      ) : (
        <h4 className="text-center">No Users</h4>
      )}
    </ListGroup>
  );
};
