import React, { useState, useContext, useEffect } from 'react';
import { GlobalContext } from '../context/GlobalState';
import { Link, useHistory } from 'react-router-dom';
import { Form, FormGroup, Label, Input, Button } from 'reactstrap';

export const Edituser = (props) => {
  const { editUser, users } = useContext(GlobalContext);
  const [selectedUser, setSelectedUser] = useState({
    id: '',
    name: '',
    email: '',
    lvl: '',
    experience: '',
  });
  const history = useHistory();
  const currentUserId = props.match.params.id;

  useEffect(() => {
    const userId = currentUserId;
    const selectedUser = users.find((user) => user.id === userId);
    setSelectedUser(selectedUser);
  }, [currentUserId, users]);

  const onSubmit = (e) => {
    e.preventDefault();
    editUser(selectedUser);
    history.push('/');
  };

  return (
    <Form onSubmit={onSubmit}>
      <FormGroup>
        <Label>Name</Label>
        <Input type="text" value={selectedUser.name} onChange={(e) => setSelectedUser({ ...selectedUser, [e.target.name]: e.target.value })} name="name" placeholder="Enter an user" required></Input>

        <Label>Email</Label>
        <Input type="text" value={selectedUser.email} onChange={(e) => setSelectedUser({ ...selectedUser, [e.target.email]: e.target.value })} name="email" placeholder="Enter an Email" required></Input>

        <Label>Level</Label>
        <Input type="text" value={selectedUser.lvl} onChange={(e) => setSelectedUser({ ...selectedUser, [e.target.lvl]: e.target.value })} name="lvl" placeholder="Enter an Level" required></Input>

        <Label>Experience</Label>
        <Input type="text" value={selectedUser.experience} onChange={(e) => setSelectedUser({ ...selectedUser, [e.target.experience]: e.target.value })} name="experience" placeholder="Enter an Experience" required></Input>
      </FormGroup>
      <Button type="submit">Edit Name</Button>
      <Link to="/" className="btn btn-danger ml-2">
        Cancel
      </Link>
    </Form>
  );
};
