import './App.css';
import React, { useState } from 'react';
import React, { useState, useContext } from 'react';
import { GlobalContext } from '../context/GlobalState';
import Table from 'react-bootstrap/Table';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import 'bootstrap/dist/css/bootstrap.min.css';
import { data } from './data.js';
export const SearchUser = () => {
  const [search, setSearch] = useState('');
  const [search1, setSearch1] = useState('');
  const [search2, setSearch2] = useState('');
  const [search3, setSearch3] = useState('');
  const { searchUser } = useContext(GlobalContext);
  console.log(search);
  return (
    <div className="App">
      <Container>
        <h1 className="text-center mt-4">Contact</h1>
        <Form>
          <InputGroup>
            <Form.Control onChange={(e) => setSearch(e.target.value)} placeholder="Search username" />
          </InputGroup>
          <InputGroup>
            <Form.Control onChange={(e) => setSearch1(e.target.value)} placeholder="Search email" />
          </InputGroup>
          <InputGroup>
            <Form.Control onChange={(e) => setSearch2(e.target.value)} placeholder="Search level" />
          </InputGroup>
          <InputGroup>
            <Form.Control onChange={(e) => setSearch3(e.target.value)} placeholder="Search experience" />
          </InputGroup>
        </Form>
        <Table striped border hover>
          <thead>
            <tr>
              <th>Username</th>
              <th>Email</th>
              <th>Level</th>
              <th>Experience</th>
            </tr>
          </thead>
          <tbody>
            {data
              .filter((user) => {
                return search.toLowerCase() === '' ? user : user.name.includes(search);
              })
              .filter((user) => {
                return search1.toLowerCase() === '' ? user : user.email.includes(search1);
              })
              .filter((user) => {
                return search2.toLowerCase() === '' ? user : user.lvl.includes(search1);
              })
              .filter((user) => {
                return search3.toLowerCase() === '' ? user : user.experience.includes(search1);
              })
              .map((user) => (
                <tr key={user.id}>
                  <td>{user.name}</td>
                  <td>{user.email}</td>
                  <td>{user.lvl}</td>
                  <td>{user.experience}</td>
                </tr>
              ))}
          </tbody>
        </Table>
      </Container>
    </div>
  );
};
